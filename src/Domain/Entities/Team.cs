﻿using Formula1.Domain.Common;
using Formula1.Domain.ValueObjects;

namespace Formula1.Domain.Entities
{
    public class Team : AuditableEntity
    {
        public int TeamId { get; set; }

        public string NameTeam { get; set; }

        public string NameFirstPilot { get; set; }

        public string NameSecondPilot { get; set; }

        public Base Base { get; set; }

        public Colour Colour { get; set; }
    }
}
