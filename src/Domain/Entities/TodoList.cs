﻿using Formula1.Domain.Common;
using Formula1.Domain.ValueObjects;
using System.Collections.Generic;

namespace Formula1.Domain.Entities
{
    public class TodoList : AuditableEntity
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public Colour Colour { get; set; } = Colour.White;

        public IList<TodoItem> Items { get; private set; } = new List<TodoItem>();
    }
}
