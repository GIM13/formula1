﻿namespace Formula1.Domain.Entities
{
    public class Base
    {
        public int BaseId { get; set; }

        public string Country { get; set; }

        public string Place { get; set; }
    }
}
