﻿using System;
using System.Threading.Tasks;
using Formula1.Application.Team.Commands;
using Formula1.Application.Teams.Commands;
using Formula1.Application.Teams.Queries;
using Formula1.Application.TodoLists.Queries.ExportTodos;
using Formula1.Application.TodoLists.Queries.GetTodos;
using Microsoft.AspNetCore.Mvc;

namespace Formula1.WebUI.Controllers
{
    //[Authorize]
    public class TeamController : ApiControllerBase
    {
        [HttpGet]
        public async Task<ActionResult<TeamVm>> Get()
        {
            return await Mediator.Send(new GetTeamQuery());
        }

        //[HttpGet("{id}")]
        //public async Task<FileResult> Get(int id)
        //{
        //    var vm = await Mediator.Send(new ExportTodosQuery { ListId = id });

        //    return File(vm.Content, vm.ContentType, vm.FileName);
        //}

        [HttpPost]
        public async Task<ActionResult<int>> Create(CreateTeamCommand command)
        {
            return await Mediator.Send(command);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Update(int id, UpdateTeamCommand command)
        {
            if (id != command.TeamId)
            {
                return BadRequest();
            }

            await Mediator.Send(command);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteTeamCommand { Id = id });

            return NoContent();
        }
    }
}
