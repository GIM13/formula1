﻿using Formula1.Application.Common.Mappings;
using Formula1.Domain.Entities;

namespace Formula1.Application.TodoLists.Queries.ExportTodos
{
    public class TodoItemRecord : IMapFrom<TodoItem>
    {
        public string Title { get; set; }

        public bool Done { get; set; }
    }
}
