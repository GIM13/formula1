﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Formula1.Application.Common.Exceptions;
using Formula1.Application.Common.Interfaces;
using Formula1.Application.TodoLists.Commands.DeleteTodoList;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Formula1.Application.Teams.Commands
{

    public class DeleteTeamCommand : IRequest
    {
        public int Id { get; set; }
    }

    public class DeleteTeamCommandHandler : IRequestHandler<DeleteTeamCommand>
    {
        private readonly IApplicationDbContext _context;

        public DeleteTeamCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(DeleteTeamCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Teams
                .Where(l => l.TeamId == request.Id)
                .SingleOrDefaultAsync(cancellationToken);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Team), request.Id);
            }

            _context.Teams.Remove(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
