﻿using System.Threading;
using System.Threading.Tasks;
using Formula1.Application.Common.Interfaces;
using MediatR;
using Formula1.Domain.ValueObjects;
using Formula1.Domain.Entities;

namespace Formula1.Application.Team.Commands
{
    public class CreateTeamCommand : IRequest<int>
    {
        //public int TeamId { get; set; }

        public string NameTeam { get; set; }

        public string NameFirstPilot { get; set; }

        public string NameSecondPilot { get; set; }

        public string BasePlace { get; set; }

        public string BaseCountry { get; set; }

        public string ColourCode { get; set; }
    }

    public class CreateTeamCommandHandler : IRequestHandler<CreateTeamCommand, int>
    {
        private readonly IApplicationDbContext _context;

        public CreateTeamCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(CreateTeamCommand request, CancellationToken cancellationToken)
        {
            var entity = new Domain.Entities.Team
            {
                //TeamId = request.TeamId,
                NameTeam = request.NameTeam,
                NameFirstPilot = request.NameFirstPilot,
                NameSecondPilot = request.NameSecondPilot,
                Base = new Base
                {
                    Country = request.BaseCountry,
                    Place = request.BasePlace,
                },
                Colour = Colour.From(request.ColourCode),

            };
            _context.Teams.Add(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return entity.TeamId;
        }
    }
}
