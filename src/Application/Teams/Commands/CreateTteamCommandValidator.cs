﻿namespace Formula1.Application.Teams.Commands
{
    using FluentValidation;
    using Formula1.Application.Team.Commands;

    public class CreateTeamCommandValidator : AbstractValidator<CreateTeamCommand>
    {
        public CreateTeamCommandValidator()
        {
            RuleFor(v => v.NameTeam)
                .MaximumLength(20)
                .MinimumLength(4)
                .NotNull()
                .NotEmpty();

            RuleFor(v => v.NameFirstPilot)
                .MaximumLength(20)
                .MinimumLength(4)
                .NotNull()
                .NotEmpty();

            RuleFor(v => v.NameSecondPilot)
                .MaximumLength(20)
                .MinimumLength(4)
                .NotNull()
                .NotEmpty();
        }
    }
}
