﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Formula1.Application.Common.Exceptions;
using Formula1.Application.Common.Interfaces;
using Formula1.Domain.Entities;
using Formula1.Domain.ValueObjects;
using MediatR;

namespace Formula1.Application.Teams.Commands
{
    public class UpdateTeamCommand : IRequest
    {
        public int TeamId { get; set; }

        public string NameTeam { get; set; }

        public string NameFirstPilot { get; set; }

        public string NameSecondPilot { get; set; }

        public string BasePlace { get; set; }

        public string BaseCountry { get; set; }

        public string ColourCode { get; set; }
    }

    public class UpdateTeamCommandHandler : IRequestHandler<UpdateTeamCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateTeamCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(UpdateTeamCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Teams.FindAsync(request.TeamId);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Team), request.TeamId);
            }

            entity.TeamId = request.TeamId;
            entity.NameTeam = request.NameTeam;
            entity.NameFirstPilot = request.NameFirstPilot;
            entity.NameSecondPilot = request.NameSecondPilot;
            entity.Base = new Base
            {
                Place = request.BasePlace,
                Country = request.BaseCountry,
            };
            entity.Colour = Colour.From(request.ColourCode);

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
