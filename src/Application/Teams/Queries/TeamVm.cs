﻿using System.Collections.Generic;
using Formula1.Application.TodoLists.Queries.GetTodos;

namespace Formula1.Application.Teams.Queries
{
    public class TeamVm
    {
        //public IList<PriorityLevelDto> PriorityLevels { get; set; }

        public IList<TeamDto> Lists { get; set; }
    }
}