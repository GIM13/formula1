﻿namespace Formula1.Application.Teams.Queries
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using AutoMapper.QueryableExtensions;
    using Formula1.Application.Common.Interfaces;
    using Formula1.Application.Common.Mappings;
    using Formula1.Application.Common.Models;
    using Formula1.Application.TodoLists.Queries.GetTodos;
    using MediatR;
    using Microsoft.EntityFrameworkCore;

    public class GetTeamQuery : IRequest<TeamVm>
    {
    }

    public class GetTeamQueryHandler : IRequestHandler<GetTeamQuery, TeamVm>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetTeamQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<TeamVm> Handle(GetTeamQuery request, CancellationToken cancellationToken)
        {
            return new TeamVm
            {
                //PriorityLevels = Enum.GetValues(typeof(PriorityLevel))
                //    .Cast<PriorityLevel>()
                //    .Select(p => new PriorityLevelDto { Value = (int)p, Name = p.ToString() })
                //    .ToList(),

                Lists = await _context.Teams
                    .AsNoTracking()
                    .ProjectTo<TeamDto>(_mapper.ConfigurationProvider)
                    .OrderBy(t => t.NameTeam)
                    .ToListAsync(cancellationToken)
            };
        }
    }
}
