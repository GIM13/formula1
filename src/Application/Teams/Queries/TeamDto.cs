﻿using Formula1.Application.Common.Mappings;
using Formula1.Domain.ValueObjects;

namespace Formula1.Application.Teams.Queries
{
    public class TeamDto : IMapFrom<Domain.Entities.Team>
    {
        public int TeamId { get; set; }

        public string NameTeam { get; set; }

        public string NameFirstPilot { get; set; }

        public string NameSecondPilot { get; set; }

        public string BasePlace { get; set; }

        public string BaseCountry { get; set; }

        public Colour Colour { get; set; }
    }
}
