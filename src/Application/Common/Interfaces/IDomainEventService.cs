﻿using Formula1.Domain.Common;
using System.Threading.Tasks;

namespace Formula1.Application.Common.Interfaces
{
    public interface IDomainEventService
    {
        Task Publish(DomainEvent domainEvent);
    }
}
