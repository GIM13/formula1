﻿using Microsoft.AspNetCore.Identity;

namespace Formula1.Infrastructure.Identity
{
    public class ApplicationUser : IdentityUser
    {
    }
}
