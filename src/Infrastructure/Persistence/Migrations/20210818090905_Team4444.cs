﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Formula1.Infrastructure.Persistence.Migrations
{
    public partial class Team4444 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BaseId",
                table: "Teams",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Bases",
                columns: table => new
                {
                    BaseId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Country = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Place = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bases", x => x.BaseId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Teams_BaseId",
                table: "Teams",
                column: "BaseId");

            migrationBuilder.AddForeignKey(
                name: "FK_Teams_Bases_BaseId",
                table: "Teams",
                column: "BaseId",
                principalTable: "Bases",
                principalColumn: "BaseId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Teams_Bases_BaseId",
                table: "Teams");

            migrationBuilder.DropTable(
                name: "Bases");

            migrationBuilder.DropIndex(
                name: "IX_Teams_BaseId",
                table: "Teams");

            migrationBuilder.DropColumn(
                name: "BaseId",
                table: "Teams");
        }
    }
}
