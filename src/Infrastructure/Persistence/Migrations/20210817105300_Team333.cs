﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Formula1.Infrastructure.Persistence.Migrations
{
    public partial class Team333 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Colour_Code",
                table: "Teams",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Colour_Code",
                table: "Teams");
        }
    }
}
