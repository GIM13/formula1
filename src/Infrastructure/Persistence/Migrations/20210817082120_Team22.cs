﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Formula1.Infrastructure.Persistence.Migrations
{
    public partial class Team22 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Colour_Code",
                table: "Teams");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Colour_Code",
                table: "Teams",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
