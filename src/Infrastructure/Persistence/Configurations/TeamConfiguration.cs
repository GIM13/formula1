﻿using System;
using Formula1.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Formula1.Infrastructure.Persistence.Configurations
{
    public class TeamConfiguration : IEntityTypeConfiguration<Team>
    {
        public void Configure(EntityTypeBuilder<Team> builder)
        {
            builder.Property(t => t.NameTeam)
                .HasMaxLength(200)
                .IsRequired();

            builder.Property(t => t.NameFirstPilot)
                .HasMaxLength(200)
                .IsRequired();

            builder.Property(t => t.NameSecondPilot)
                .HasMaxLength(200)
                .IsRequired();

            builder.Property(t => t.TeamId)
                .HasMaxLength(200)
                .IsRequired();

            builder
                .OwnsOne(b => b.Colour);

            //builder
            //    .OwnsOne(b => b.Base);
        }
    }
}
