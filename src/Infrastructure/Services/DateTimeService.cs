﻿using Formula1.Application.Common.Interfaces;
using System;

namespace Formula1.Infrastructure.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
